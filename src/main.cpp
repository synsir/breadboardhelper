#include <synos.h>

#include "../stmbase/drivers/tm1638.h"
#include "../stmbase/drivers/ssd1306/ssd1306.h"

TM1638 multikey;
syn::Gpio dio_pins[8];

#define ADC_AVG_COUNT 32
uint8_t adc_averages[ADC_AVG_COUNT];
uint8_t *adc_avg_loc = adc_averages;

uint8_t voltage_smoother()
{
  uint16_t voltage_16 = syn::Adc::read();
  uint8_t voltage = voltage_16 >> 2;
  uint8_t volt_bits = voltage_16 & 0x3;
  if (volt_bits == 0 && voltage != 0)
  {
    --voltage;
  }
  else if (volt_bits == 3 && voltage != 255)
  {
    ++voltage;
  }
  uint16_t avg = 0;
  for (uint8_t i = 0; i < ADC_AVG_COUNT; ++i)
  {
    avg += adc_averages[i];
  }
  avg = avg * 3 + voltage;
  *adc_avg_loc = voltage;
  if (++adc_avg_loc == &adc_averages[ADC_AVG_COUNT])
  {
    adc_avg_loc = adc_averages;
  }
  voltage = avg / (ADC_AVG_COUNT * 3 + 1);

  // uint8_t voltage = avg / ADC_AVG_COUNT;
  // if (prev_volt > voltage)
  // {
  //   if ((prev_volt - voltage) < 2)
  //   {
  //     voltage = prev_volt;
  //   }
  // }
  // else
  // {
  //   if ((voltage - prev_volt) < 2)
  //   {
  //     voltage = prev_volt;
  //   }
  // }
  return voltage;
}

uint8_t key_reader(bool &output_enable)
{
  static uint16_t pressed_counter = 0;

  uint8_t keys = multikey.readKeys();
  if (keys == 0xC0)
  {
    if (++pressed_counter == 5000)
    {
      if (output_enable == true)
      {
        multikey.intensity(0); // turn off display completely
        output_enable = false;
      }
      else
      {
        multikey.intensity(); // turn display default intensity
        output_enable = true;
      }
    }
  }
  else
  {
    pressed_counter = 0;
  }

  return keys;
}

void tm1638_update(uint16_t arg)
{
  syn::Adc::initContinuous(2);
  syn::Adc::convert();

  multikey.init('A', 3, 'A', 2, 'A', 1);

  // wait some time before disabling SWIM interface to use it
  // as a regular GPIO
  //for (uint8_t i = 0xFE; i != 0xFF; --i)
  for (uint8_t i = 0x0F; i != 0xFF; --i)
  {
    multikey.setLed(i);
    // load also synchronizes display, so write LED first
    multikey.writeLoad(i);
    syn::Routine::sleep(100);
  }
  multikey.sync();                            // delete display output
  uint8_t text[4] = {0x77, 0x5E, 0x58, 0x00}; // Adc_
  multikey.writeLeft(text);
  // disable swim and setup DIO pins
  //syn::System::setSwim(false);
  dio_pins[0].init('D', 4).input_pullup().clear();
  dio_pins[1].init('D', 3).input_pullup().clear();
  dio_pins[2].init('D', 2).input_pullup().clear();
  dio_pins[3].init('D', 1).input_pullup().clear();
  dio_pins[4].init('C', 7).input_pullup().clear();
  dio_pins[5].init('C', 6).input_pullup().clear();
  dio_pins[6].init('C', 5).input_pullup().clear();
  dio_pins[7].init('C', 3).input_pullup().clear();

  uint8_t prev_leds = 0;
  uint8_t prev_volt = 0;
  bool multikey_output = true;
  while (1)
  {
    uint8_t voltage = voltage_smoother();

    syn::Uart::putc(voltage);

    uint8_t keys = key_reader(multikey_output);
    uint8_t leds = 0;
    syn::Gpio *pdio = dio_pins;
    for (uint8_t mask = 0x01; mask != 0; mask <<= 1, ++pdio)
    {
      if (keys & mask)
      {
        pdio->set_direction_bit(); // push-pull low
      }
      else
      {
        pdio->clear_direction_bit(); // input_pullup
        syn::Utility::udelay(5);
        if (pdio->read())
        {
          leds |= mask;
        }
      }
    }

    multikey.setLed(~leds); // led active low
    multikey.writeRight(voltage);
    if(prev_leds != leds || prev_volt != voltage)
    {
      if (multikey_output)
      {
        prev_leds = leds;
        prev_volt = voltage;
        multikey.sync();
      }
    }

    syn::Routine::yield();
  }
}

char uartbuf[32];


void ssd1306_update(uint16_t arg)
{
  SSD1306::Display disp(1, SSD1306::Display::I2CADDRESS_A);
  disp.init(syn::I2c::medium);
  disp.flip(true);
  
  //syn::Uart::read_async(uartbuf, 32);
  uint16_t counter = 10000;
  while (1)
  {
    // disp.write_line(0, "Line 1: YOLO~!*&$", &SSD1306::font_runescape_16);
    // syn::Routine::sleep(1);

    // disp.write_line(2, "Line 2: gagaga___?", &SSD1306::font_runescape_16);
    // syn::Routine::sleep(1);

    // char *pw = syn::Utility::strcpy(uartbuf, "Line 3: Yyol! ");
    // pw = syn::Utility::sprint_u16(pw, ++counter);
    // *pw = 0;
    // disp.write_line(4, uartbuf, &SSD1306::font_runescape_16);
    // syn::Routine::sleep(1);

    // pw = syn::Utility::strcpy(uartbuf, "Line 4: Meg@ ");
    // pw = syn::Utility::sprint_u16(pw, (counter % 10000) * 150);
    // *pw = 0;
    // disp.write_line(6, uartbuf, &SSD1306::font_runescape_16);
    syn::Routine::sleep(1);
  }
}

int main()
{
  syn::Kernel::init();
  
  syn::Uart::init(syn::Uart::bd115200);

  // IMPORTANT !!!
  // the count of routines has to exactly match the amount of routines used
  // the routine index given during initialization has to match

  // Routines should never return, but there is a safeguard if that happens.
  // However that safeguard costs 2 bytes of stack and 6 bytes of rom.
  // furthermore, if round robin is disabled, the processor will be stuck on the
  // returned routine forever. you have been warned.

  syn::Routine::init(&tm1638_update, 0, 128);
  syn::Routine::init(&ssd1306_update, 0, 255);
  //syn::Routine::init(&analog_update, 0, 96);

  //syn::SysTickHook::init<0, 10>(tim_x);
  //syn::SysTickHook::init<1, 10>(tim_y);

  // never returns
  syn::Kernel::spin();
  return 0;
}
